var config = require("./env");
var templateUrl = require('ngtemplate-loader!html-loader!./tags-template.html');

angular.module(
  'nspa-tags', []
)
.service("Tag", ['$http', function($http){
    var self =this;
    self.data = [];
    self.appToken = "";
    self.resourceData = {
      resource_name : "",
      resource_id : "",
      app : ""
    }


    self.getTags  = function()
    {
        return $http({
          method : "GET",
          url : "/v1/api/tags?app="+self.resourceData.app+"&resource_id="+self.resourceData.resource_id+"&resource_name="+self.resourceData.resource_name
        }).then(function(response){
          self.data = response.data.data;
        });
    };

    self.store = function(data){
      console.log(data);
        return $http({
          method : "POST",
          url : "/v1/api/tags",
          data : data
        }).then(function(response){
          self.getTags();
        }, function(){

        });
    };

}])
.directive('nspaTag', [ '$http', 'Tag',
function($http, Tag){
  return {
    restrict : "EA",
    templateUrl : templateUrl,
    scope : {
      resourceType : "@",
      resourceId : "=",
      appName : "@",
      appToken : "@"
    },
    link : function($scope, element, attrs)
    {
      console.log(attrs);
      console.log("-- SCOPE --");
      console.log($scope);

        $scope.TagService = Tag;
        $scope.form = {
          value : "",
          type: "",
          resource_name : attrs.resourceType,
          resource_id : $scope.resourceId,
          app : attrs.appName
        };
        Tag.appToken = attrs.appToken;
        Tag.resourceData = {
          resource_name : attrs.resourceType,
          resource_id : $scope.resourceId,
          app : attrs.appName
        };


        $scope.$watch('resourceId', function() {
            Tag.getTags().then(function(){

            });
      });

        $scope.saveTag = function(){

            Tag.store($scope.form);
            alert("Saved");
        }
    }
  };
}]);
